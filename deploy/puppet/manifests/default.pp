# Basic Puppet manifest

Exec { path => [ "/bin/", "/sbin/", "/usr/bin/", "/usr/sbin/" ] }

user { "deploy":
    gid        => "deploy",
    managehome => true,
    ensure     => "present",
}

class system-update {

  exec { 'apt-get update':
    command => 'apt-get update',
  }

  $sysPackages = [ "build-essential", "vim", "curl", "git" ]
  package { $sysPackages:
    ensure => "installed",
    require => Exec['apt-get update'],
  }
}


class capistrano-setup {
  
  include capistrano

  capistrano::deploytarget {'myapp':
    deploy_user    => 'deploy',
    share_group    => 'deploy',
    deploy_dir     => '/var/www/myapp/', 
    shared_dirs    => ['log', 'tmp', 'public', 'tmp/pids', 'public/system',  'public/assets', 'tmp/cache'], 
    #$cap_version   => 3,
  }

}
u
class nginx-setup {

  include nginx

  nginx::resource::upstream { 'myapp_rack_app':
    members => [
      'localhost:3000',
    ],
  }
  
  nginx::resource::vhost { 'myapp.com':
    proxy => 'http://myapp_rack_app',
  }
}

include system-update
include capistrano-setup
include nginx-setup
include unicorn



